# Unity Commons - Visual Scripting

===

THE REPOSITORY HAS BEEN MERGED WITH OTHER UTILITY REPOSITORIES INTO THE CENTRALIZED REPOSITORY at https://bitbucket.org/hsandt/hyper-unity-commons

It is however preserved so we can still access the version history. We may eventually open a new centralized repository that has both the version history of each original repository, and the latest scripts.

===

A collection of helper scripts for Unity Visual Scripting.

Requires the Visual Scripting (ex-Bolt) package installed in your project.

I keep this repository to use as Git submodule in my personal and team projects. I consider most scripts to be stable,
but I regularly add features and update the API to fit new versions of Unity, or the needs of my current project;
sometimes breaking compatibility with previous projects.

For this reason, I recommend people who are interested in the scripts but not working directly with me to either

* download a frozen copy of master branch and stick to it for a given project
* or download and adapt individual scripts as they need (just copy the MIT LICENSE along)

Improvement suggestions are welcome. I don't take pull requests at the moment, but you can reach me at
hs@gamedesignshortcut.com.